import { playAudioFile } from 'audic';
import dayjs from 'dayjs';
import * as fs from 'fs';
import { SpeechRecorder } from 'speech-recorder';
import { cfg } from './cfg';
import { calculateMedian, convertRawToAudioFile } from './ffmpeg';
import { messages } from './messages';
import { openai, webStreamToNodeStream } from './openai';

let writeStreamName: string;
let writeStream: fs.WriteStream | undefined;
let allProbabilities: number[] = [];
export let playing = false;


// https://www.npmjs.com/package/speech-recorder?activeTab=readme
export const recorder = new SpeechRecorder({
    device: cfg.recorder.audioDevice,
    sampleRate: cfg.recorder.sampleRate,
    onChunkStart: ({ audio }) => {
        if (playing) { return; }
        console.log(Date.now(), "Chunk start");

        allProbabilities = [];

        const date = dayjs();
        writeStreamName = `audio/mic-${ date.format('YYYY-MM-DD-HHmmss') }.raw`;
        writeStream = fs.createWriteStream(writeStreamName);
        const buffer = Buffer.from(audio.buffer, audio.byteOffset, audio.byteLength);
        writeStream.write(buffer);
    },
    onAudio: ({ speaking, probability, volume, audio, speech }) => {
        console.log(Date.now(), speaking, probability, volume, speech);
        // if (speaking) {
        // }

        if (writeStream && !playing) {
            allProbabilities.push(probability);
            const buffer = Buffer.from(audio.buffer, audio.byteOffset, audio.byteLength);
            // console.log(`Writing audio buffer, length: ${buffer.length}`);
            writeStream.write(buffer);
        }
    },
    onChunkEnd: () => {
        if (!writeStream) { return; }
        const median = calculateMedian(allProbabilities);
        const name = writeStreamName;

        if (playing) {
            return;
        }

        if (median > 0.1) {
            console.log(`Speech Success ${name}`);
            playing = true;
            writeStream.end(() => {
                console.log(`${name} successfully written`);

                convertRawToAudioFile(writeStreamName)
                .then(async (outputName) => {
                    fs.unlink(writeStreamName, () => {});
                    const readStream = fs.createReadStream(outputName);
                    const transcription = await openai.audio.transcriptions.create({
                        model: 'whisper-1',
                        file: readStream,
                        response_format: 'text',
                    });

                    console.log(`Jared: `, transcription);
                    messages.push({
                        role: 'user',
                        name: 'Jared',
                        content: transcription as any as string,
                    });

                    // console.log(messages);

                    const completion = await openai.chat.completions.create({
                        model: 'gpt-4-1106-preview',
                        messages,
                    });
                    const choice = completion.choices[0].message;
                    if (!choice?.content) { return; }

                    console.log(`Billy: `, choice.content);

                    messages.push({
                        role: 'assistant',
                        content: choice.content,
                        name: 'Billy',
                    });

                    const speechResponse = await openai.audio.speech.create({
                        model: 'tts-1',
                        response_format: 'mp3',
                        voice: 'onyx',
                        speed: 1.2,
                        input: choice.content,
                    });

                    if (!speechResponse?.body) {
                        return;
                    }
                    const nodeStream = webStreamToNodeStream(speechResponse.body);

                    const billyOutputName = name.replace('.raw', '-output.mp3');

                    const speechResponseFileStream = fs.createWriteStream(billyOutputName);
                    nodeStream.pipe(speechResponseFileStream);

                    speechResponseFileStream.on('finish', () => {
                        console.log(`Audio written to file ${ billyOutputName }`);

                        // sudo apt-get install mpg123
                        playAudioFile(billyOutputName).then(() => {
                            console.log('Audio playback finished');
                        }).catch((err) => {
                            console.error('Error playing file:', err);
                        }).finally(() => {
                            playing = false;
                        })
                        // player.play(billyOutputName, (err) => {
                        //     playing = false;
                        //     if (err) {
                        //         console.error('Error playing file:', err);
                        //     } else {
                        //         console.log('Audio playback finished');
                        //     }
                        // })  
                    });
                    
                    speechResponseFileStream.on('error', (err) => {
                        console.error('Error writing file:', err);
                        if (writeStream) {
                            writeStream.end();
                            writeStream = undefined;
                        }
                        playing = false;
                    });

                    // console.log(response);
                    // response.body?.pipeThrough(decoder.)
    
                    // opusStream.on('data', (chunk) => {
                    //     const pcmData = decoder.decode(chunk);
                    //     speaker.write(pcmData);
                    // });
                
                    // opusStream.on('end', () => {
                    //     console.log('Audio playback finished');
                    // });
                    // const audioUrl = response.audio_url; // Replace with actual property name
                    // await playOpusAudio(response.body);
                    // speaker.
                    
                }).catch((err) => {
                    playing = false;
                    if (writeStream) {
                        writeStream.end();
                        writeStream = undefined;
                    }
                })


            });
        } else {
            console.log(`Chunk end - ${median}`);

            writeStream.end(() => {
                // delete
                fs.unlink(name, () => {

                });
            });
        }

        writeStream = undefined;

    },
});
