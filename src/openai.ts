import { OpenAI } from 'openai';
import { Readable } from 'stream';
import { cfg } from './cfg';
// import { default as PlaySound } from 'play-sound';
// import { playAudioFile } from 'audic';

export const openai = new OpenAI({
    apiKey: cfg.openai.secret,
});


// Convert a Web ReadableStream to a Node.js Readable stream
export function webStreamToNodeStream(webStream) {
    const nodeStream = new Readable({
        read() {}
    });

    webStream.on('data', chunk => {
        nodeStream.push(chunk);
    });

    webStream.on('end', () => {
        nodeStream.push(null);
    });

    webStream.on('error', err => {
        nodeStream.emit('error', err);
    });

    return nodeStream;
}
