import { default as ffmpegStatic } from 'ffmpeg-static';
import { default as ffmpeg } from 'fluent-ffmpeg';


export function loadFFMPEG() {

    if (!ffmpegStatic) {
        throw new Error(`Cannot find ffmpeg`);
    }

    ffmpeg.setFfmpegPath(ffmpegStatic);
}


export function convertRawToAudioFile(
    inputPath: string,
    format = 'wav'
) {
    const outputPath = inputPath.replace('.raw', `.${ format }`);

    return new Promise<string>((resolve, reject) => {
        ffmpeg(inputPath)
            .inputFormat('s16le') // 16-bit PCM, little-endian
            .audioCodec('libmp3lame')
            .audioBitrate('96k') // Explicitly set a bit rate
            .inputOptions('-ar 16000') // Explicitly set input sample rate
            .audioChannels(1) // 1 channel (mono)
            .toFormat(format)
            .output(outputPath)
            .on('end', () => {
                // console.log('Conversion finished.');
                resolve(outputPath);
            })
            .on('error', (err) => {
                console.error('Error:', err);
                reject(err);
            })
            .on('stderr', (stderrLine) => {
                // console.log('FFmpeg Output:', stderrLine);
            })
            .run();
    });
}


export function calculateMedian(values: number[]): number {

    if (values.length === 0) {
        throw new Error('Input array is empty');
    }

    // Sorting values, preventing original array
    // from being mutated.
    values = [...values].sort((a, b) => a - b);

    const half = Math.floor(values.length / 2);

    return (values.length % 2
        ? values[half]
        : (values[half - 1] + values[half]) / 2
    );

}