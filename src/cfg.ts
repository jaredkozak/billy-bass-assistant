import { UNDEFINED_STRING, environment, required } from "@kozakatak/environment-loader";
import 'dotenv/config';

export const cfg = environment({

    openai: {
        secret: required(UNDEFINED_STRING),
        defaultTemperature: 0.2,
        embeddingModel: 'text-embedding-ada-002',
    },

    recorder: {
        sampleRate: 16000,
        audioDevice: 22,
        showDevices: true,
    },

    player: {

    },

    tz: 'America/Winnipeg',
}, {});
