import { createInterface } from 'node:readline';
import { devices } from 'speech-recorder';
import { cfg } from './cfg';
import { loadFFMPEG } from './ffmpeg';
import { recorder } from './recorder';


let running = true;

function run() {

    if (!running) {
        process.exit(0);
    }

    setTimeout(run, 100);
}

function start() {
    loadFFMPEG();

    const readline = createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    
    readline.on('line', (input) => {
    
        console.log(`Exiting`);
        running = false;
        recorder.stop();
    });
    
    recorder.start();
    // player.play('audio/mic-2024-04-23-194246.wav');
    console.log(`Started`);

    if (cfg.recorder.showDevices) {
        console.log(devices())
    }
    run();
}

start();
